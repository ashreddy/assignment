const fs = require('fs');  //importing fs module

fs.readFile("demo.txt", "utf-8",(err,data)=>{     //filename,encoding,function
if(err){
    console.log(err)
}
console.log(data)

})     

//dynamic creation of a file
 fs.writeFile("example.html","'utf-8",(err)=>{
     if(err){
         console.log(err)
     }
     console.log("the file created successfully")
 })

//to write content in the file which is created successfully
  const sample="console.log()"
  fs.writeFile("example.html", sample,(err)=>{         //filename,content stored variable,function
    if (err){
      console.log(err)
    }
    console.log("content created successfully")
  })            
  //content added into the file
  fs.appendFile("demo.txt","batch 62", (err) => {
    if(err){
        console.log(err);
    }
    console.log("data added to file");
});

//rename the filename
  fs.rename('example.html','changedfile.js',(err)=>{
      if(err){
          console.log(err)
      }
      console.log("renamed the file name successfully")
   })


//delete a file
fs.unlink('changedFile.js',(err)=>{
    if(err){
        console.log(err)
    }
    console.log("deleted file successfully")
})